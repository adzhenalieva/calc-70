const initialState = {
    math: ''
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case 'ADD_CALC':
            return {
                ...state, math: state.math + action.number
            };
        case 'MAKE_CALC':
            return {
                ...state, math: String(eval((state.math)))
            };
        case 'REMOVE_CALC':
            const newMATH = state.math.substr(0, state.math.length - 1);
            return {
                ...state, math: newMATH
            };
        default:
            return state;
    }

};


export default reducer;