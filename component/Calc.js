import React from 'react';
import {StyleSheet, Button, View, Text, TouchableOpacity} from 'react-native';
import {connect} from "react-redux";

const styles = StyleSheet.create({
    NewContainer: {
        flex: 2,
        backgroundColor: '#fff',
        alignItems: 'stretch',
        justifyContent: "center",
    },
    button: {
        backgroundColor: 'yellow',
        paddingHorizontal: 20,
        width: 90
    },
    button1: {
        backgroundColor: '#DDDDDD',
        paddingHorizontal: 20,
        width: 90
    },
    button2: {
        backgroundColor: 'red',
        paddingHorizontal: 20,
        width: 90
    },
    button3: {
        backgroundColor: '#DDDDDD',
        paddingHorizontal: 20,
        width: 330,
        alignItems: 'center',
        justifyContent: "center",
    },
    text: {
        borderWidth: 1,
        padding: 10,
        borderColor: 'yellow',
        height: 110,
        margin: 10,

    },
    text1: {
        fontSize: 25,
        padding: 10,
        alignItems: 'center'
    },
    row: {
        flexDirection: 'row',
        alignItems: 'stretch',
        margin: 10,
        justifyContent: "space-between",
    }
});

class Calc extends React.Component {
    render() {
        return (
            <View style={styles.NewContainer}>
                <View style={styles.text}><Text style={styles.text1}>{this.props.math}</Text></View>
                <View style={styles.row}>
                    <TouchableOpacity style={styles.button} onPress={() => this.props.addCalc(7)}><Text
                        style={styles.text1}>7</Text></TouchableOpacity>
                    <TouchableOpacity style={styles.button} onPress={() => this.props.addCalc(8)}><Text
                        style={styles.text1}>8</Text></TouchableOpacity>
                    <TouchableOpacity style={styles.button} onPress={() => this.props.addCalc(9)}><Text
                        style={styles.text1}>9</Text></TouchableOpacity>
                </View>
                <View style={styles.row}>
                    <TouchableOpacity style={styles.button} onPress={() => this.props.addCalc(4)}><Text
                        style={styles.text1}>4</Text></TouchableOpacity>
                    <TouchableOpacity style={styles.button} onPress={() => this.props.addCalc(5)}><Text
                        style={styles.text1}>5</Text></TouchableOpacity>
                    <TouchableOpacity style={styles.button} onPress={() => this.props.addCalc(6)}><Text
                        style={styles.text1}>6</Text></TouchableOpacity>
                </View>
                <View style={styles.row}>
                    <TouchableOpacity style={styles.button} onPress={() => this.props.addCalc(1)}><Text
                        style={styles.text1}>1</Text></TouchableOpacity>
                    <TouchableOpacity style={styles.button} onPress={() => this.props.addCalc(2)}><Text
                        style={styles.text1}>2</Text></TouchableOpacity>
                    <TouchableOpacity style={styles.button} onPress={() => this.props.addCalc(3)}><Text
                        style={styles.text1}>3</Text></TouchableOpacity>
                </View>
                <View style={styles.row}>

                    <TouchableOpacity style={styles.button1} onPress={() => this.props.addCalc('+')}><Text
                        style={styles.text1}>+</Text></TouchableOpacity>
                    <TouchableOpacity style={styles.button} onPress={() => this.props.addCalc(0)}><Text
                        style={styles.text1}>0</Text></TouchableOpacity>
                    <TouchableOpacity style={styles.button1} onPress={() => this.props.addCalc('-')}><Text
                        style={styles.text1}>-</Text></TouchableOpacity>
                </View>
                <View style={styles.row}>
                    <TouchableOpacity style={styles.button1} onPress={() => this.props.addCalc('*')}><Text
                        style={styles.text1}>*</Text></TouchableOpacity>
                    <TouchableOpacity style={styles.button1} onPress={() => this.props.addCalc('/')}><Text
                        style={styles.text1}>/</Text></TouchableOpacity>
                    <TouchableOpacity style={styles.button2} onPress={this.props.removeCalc}><Text style={styles.text1}>C</Text></TouchableOpacity>
                </View>
                <View style={styles.row}>
                    <TouchableOpacity style={styles.button3} onPress={this.props.makeCalc}><Text
                        style={styles.text1}>=</Text></TouchableOpacity>
                </View>
            </View>
        );
    }
}

const mapStateToProps = state => {
    return {
        math: state.math
    };
};

const mapDispatchToProps = dispatch => {
    return {
        addCalc: number => dispatch({type: 'ADD_CALC', number}),
        makeCalc: () => dispatch({type: 'MAKE_CALC'}),
        removeCalc: () => dispatch({type: 'REMOVE_CALC'})
    };

};


export default connect(mapStateToProps, mapDispatchToProps)(Calc);
